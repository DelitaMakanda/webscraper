try:
    from urllib.request import urlopen
    from urllib.error import HTTPError
except ImportError:
    from urllib2 import urlopen

from bs4 import BeautifulSoup

def getTitle(url):
    try:
        html = urlopen(url)
    except HTTPError as e:
        return None
    try:
        bsObj = BeautifulSoup(html.read(), "html.parser")
        title = bsObj.body.h1
        appartList = bsObj.findAll("span", {"class": "h1"} )
    except AttributeError as e:
        return None
    return title, appartList

title, appartList =  getTitle("http://www.pap.fr/annonce/vente-appartements-paris-75-g439-du-studio-au-2-pieces-jusqu-a-40-m2")
if title == None:
    print('title not found')
else:
    print(title)

for appart in appartList:
    print(appart.get_text())
