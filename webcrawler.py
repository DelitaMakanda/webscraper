try:
    from urllib.request import urlopen
    from urllib.error import HTTPError
except ImportError:
    from urllib2 import urlopen


from bs4 import BeautifulSoup
import re
import random
import datetime

random.seed(datetime.datetime.now())

pages = set()
def getPages(pagesUrl):
    global pages
    html = urlopen("http://www.pap.fr/annonce" + pagesUrl)
    bsObj = BeautifulSoup(html, "html.parser")
    try:
        print(bsObj.h1.get_text())
        print(bsObj.find_all('div', class_="box").find('a').attrs['href'])
        print(bsObj.find_all('div', class_="box").findAll('span', {'class': 'h1'})[0])
        print(bsObj.find_all('div', class_="box").findAll('span', {'class': 'price'})[0])
    except AttributeError as e:
        print('no pages here')
        
    for link in bsObj.findAll('a', href=re.compile("^(/annonce/)")):
        if 'href' in link.attrs:
            if link.attrs['href'] not in pages:
                newPage = link.attrs['href']
                print("------------------------" + newPage)
                pages.add(newPage)
                getPages(newPage)

getPages("")


def getLinks(appartUrl):
    html = urlopen("http://www.pap.fr/annonce" + appartUrl)
    bsObj = BeautifulSoup(html, "html.parser")
    return bsObj.find('div', {'class': 'box'}).findAll('a', href=re.compile("^(/annonce/)((?!:).)*$"))

links = getLinks("/vente-appartements-paris-75-g439-du-studio-au-2-pieces-jusqu-a-40-m2")
while len(links) > 0:
    newAppart = links[random.randint(0, len(links) - 1)].attrs['href']
    print(newAppart)
    links = getLinks(newAppart)
